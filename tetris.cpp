#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/Window.hpp>
#include <algorithm>
#include <cassert>
#include <cstdint>
#include <functional>
#include <iostream>
#include <random>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>
using std::size_t;
using namespace sf;
using vec2 = std::pair<int, int>;
template <typename T, typename U, typename T2, typename U2>
std::pair<decltype(std::declval<T>() + std::declval<T2>()),
          decltype(std::declval<U>() + std::declval<U2>())>
operator+(const std::pair<T, U> &p1, const std::pair<T2, U2> &p2) {
  return std::make_pair(p1.first + p2.first, p1.second + p2.second);
}
template <typename numeric_t1, typename numeric_t2>
void rotateCCW(std::pair<numeric_t1, numeric_t2> &p) {
  std::swap(p.first, p.second);
  p.second = -p.second;
}
template <typename numeric_t1, typename numeric_t2>
void rotateCW(std::pair<numeric_t1, numeric_t2> &p) {
  std::swap(p.first, p.second);
  p.first = -p.first;
}
std::vector<std::pair<Color, vec2>> obs(0);
template <size_t size> struct trisshape {
  std::array<vec2, size> positions;
  vec2 abspos;
  Color color;
  constexpr trisshape(const std::array<vec2, size> &p)
      : positions(p), abspos(0, 0), color(Color::Green) {}
  constexpr trisshape(const std::initializer_list<vec2> &p)
      : abspos(4, -2), color(Color::Green) {
    size_t i = 0;
    assert(p.size() == size);
    for (auto &x : p) {
      positions[i++] = x;
    }
  }
  bool positionOutOfBoundsOrIntersect() {
    for (int i = 0; i < positions.size(); i++) {
      vec2 ps = positions[i] + abspos;
      if (ps.first >= 10 || ps.first < 0) {
        return true;
      }
      if (ps.second >= 20) {
        return true;
      }
      for (int ih = 0; ih < obs.size(); ih++) {
        if ((ps.first == obs[ih].second.first) &&
            (ps.second == obs[ih].second.second)) {
          return true;
        }
      }
    }
    return false;
  }
  bool shiftLeft(bool check = true) {
    abspos.first--;
    if (check && positionOutOfBoundsOrIntersect()) {
      shiftRight(false);
      return false;
    }
    return true;
  }
  bool shiftRight(bool check = true) {
    abspos.first++;
    if (check && positionOutOfBoundsOrIntersect()) {
      shiftLeft(false);
      return false;
    }
    return true;
  }
  bool rotateCW(bool check = true) {
    std::for_each(positions.begin(), positions.end(),
                  [](vec2 &p) { ::rotateCW(p); });
    if (check && positionOutOfBoundsOrIntersect()) {
      rotateCCW(false);
      return false;
    }
    return true;
  }
  bool rotateCCW(bool check = true) {
    std::for_each(positions.begin(), positions.end(),
                  [](vec2 &p) { ::rotateCCW(p); });
    if (check && positionOutOfBoundsOrIntersect()) {
      rotateCW(false);
      return false;
    }
    return true;
  }
  bool fall() {
    abspos.second++;
    if (positionOutOfBoundsOrIntersect()) {
      abspos.second--;
      return false;
    }
    return true;
  }
};
int a[]{6, 3, 3};
trisshape<4> pieces[] = {
    {{0, -1}, {0, 0}, {0, 1}, {1, 1}},   {{0, -1}, {0, 0}, {0, 1}, {-1, 1}},
    {{-1, -1}, {0, -1}, {0, 0}, {1, 0}}, {{-1, 0}, {0, 0}, {0, -1}, {1, -1}},
    {{-1, 0}, {0, 0}, {0, -1}, {0, 1}},  {{-1, 0}, {0, 0}, {-1, -1}, {0, -1}},
    {{-1, 0}, {0, 0}, {1, 0}, {2, 0}},   {{-1, 0}, {0, 0}, {1, 0}, {2, 0}},
    {{-1, 0}, {0, 0}, {1, 0}, {2, 0}}};
Color colors[] = {Color(255, 0, 0),    Color(0, 255, 0),   Color(0, 0, 255),
                  Color(255, 255, 0),  Color(255, 120, 0), Color(0, 255, 255),
                  Color(255, 50, 120), Color(0, 120, 180), Color(70, 100, 180)};
int main() {

  bool skeydown = false;
  int fallcounter = 20;
  unsigned long long a;
  std::random_device dev;
  std::mt19937_64 gen(dev());
  std::uniform_int_distribution<size_t> dis(0, 6);
  RenderWindow window(VideoMode(800, 1000), "SFML window");
  window.setVerticalSyncEnabled(true);
  window.setActive();
  std::vector<std::uint8_t> pixels(64 * 64 * 40);
  std::generate(pixels.begin(), pixels.end(), std::ref(gen));
  for (auto &x : pixels) {
    x |= (255 << 24);
  }
  window.setIcon(64, 64, pixels.data());
  int kds;
  trisshape<4> fallingPiece = pieces[kds = dis(gen)];
  fallingPiece.color = colors[kds];
  trisshape<4> nextPiece = pieces[kds = dis(gen)];
  nextPiece.color = colors[kds];
  while (window.isOpen()) {
    Event event;
    while (window.pollEvent(event)) {
      if (event.type == Event::Closed)
        window.close();
      if (event.type == Event::KeyPressed) {
        if (event.key.code == Keyboard::Escape) {
          window.close();
        }
        if (event.key.code == Keyboard::W || event.key.code == Keyboard::Up) {
          fallingPiece.rotateCCW();
        }
        if (event.key.code == Keyboard::A || event.key.code == Keyboard::Left) {
          fallingPiece.shiftLeft();
        }
        if (event.key.code == Keyboard::LShift) {
          fallingPiece.rotateCW();
        }
        if (event.key.code == Keyboard::D ||
            event.key.code == Keyboard::Right) {
          fallingPiece.shiftRight();
        }
        if (event.key.code == Keyboard::S || event.key.code == Keyboard::Down) {
          skeydown = true;
        }
        if (event.key.code == Keyboard::Space) {
          while (fallingPiece.fall());
          for (size_t i = 0; i < fallingPiece.positions.size(); i++) {
            obs.push_back({fallingPiece.color,
                           fallingPiece.positions[i] + fallingPiece.abspos});
          }
          for (int i = 0; i < 20; i++) {
            int count = 0;
            for (auto &x : obs) {
              if (x.second.second == i)
                count++;
            }
            if (count == 10) {
              for (auto it = obs.begin(); it != obs.end();) {
                if (it->second.second == i) {
                  it = obs.erase(it);
                } else {
                  if (it->second.second < i)
                    (it->second.second)++;
                  ++it;
                }
              }
            }
          }
          std::swap(fallingPiece, nextPiece);
          nextPiece = pieces[kds = dis(gen)];
          nextPiece.color = colors[kds];
        }
      }

      if (event.type == Event::KeyReleased) {
        skeydown = false;
      }
    }
    if (skeydown) {
      fallcounter -= 5;
    }
    window.clear(Color::Black);
    for (int i = 0; i < fallingPiece.positions.size(); i++) {
      vec2 pos = fallingPiece.positions[i] + fallingPiece.abspos;
      RectangleShape sh(Vector2f(48, 48));
      sh.setPosition(Vector2f(pos.first * 50, pos.second * 50));
      sh.setFillColor(fallingPiece.color);
      sh.setOutlineColor(Color::Black);
      sh.setOutlineThickness(2);
      window.draw(sh);
    }
    for (int i = 0; i < nextPiece.positions.size(); i++) {
      vec2 pos = nextPiece.positions[i] + std::make_pair<int,int>(12,3);
      RectangleShape sh(Vector2f(48, 48));
      sh.setPosition(Vector2f(pos.first * 50, pos.second * 50));
      sh.setFillColor(nextPiece.color);
      sh.setOutlineColor(Color::Black);
      sh.setOutlineThickness(2);
      window.draw(sh);
    }
    for (int i = 0; i < obs.size(); i++) {
      vec2 pos = obs[i].second;
      RectangleShape sh(Vector2f(48, 48));
      sh.setPosition(Vector2f(pos.first * 50, pos.second * 50));
      sh.setOutlineColor(Color::Black);
      sh.setOutlineThickness(2);
      sh.setFillColor(obs[i].first);
      window.draw(sh);
    }
    if ((--fallcounter) <= 0) {
      fallcounter = 30;
      if (!fallingPiece.fall()) {
        for (size_t i = 0; i < fallingPiece.positions.size(); i++) {
          obs.push_back({fallingPiece.color,
                         fallingPiece.positions[i] + fallingPiece.abspos});
        }
        for (int i = 0; i < 20; i++) {
          int count = 0;
          for (auto &x : obs) {
            if (x.second.second == i)
              count++;
          }
          if (count == 10) {
            for (auto it = obs.begin(); it != obs.end();) {
              if (it->second.second == i) {
                it = obs.erase(it);
              } else {
                if (it->second.second < i)
                  (it->second.second)++;
                ++it;
              }
            }
          }
        }
        std::swap(fallingPiece, nextPiece);
        nextPiece = pieces[kds = dis(gen)];
        nextPiece.color = colors[kds];
      }
    }

    RectangleShape a(Vector2f(500, 1000));
    a.setOutlineColor(Color(255,255,255));
    a.setOutlineThickness(2);
    a.setFillColor(Color(0,0,0,0));
    a.setPosition(0, 0);

    window.draw(a);
    window.setActive();
    window.display();

  }
}
